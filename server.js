var fs = require('fs');
var _ = require('lodash');
var path = require('path');
var express = require('express');
var expressJwt = require('express-jwt');
var bodyParser = require('body-parser');
var morgan = require('morgan');
var storage = require('node-persist');
var jwt = require('jsonwebtoken');
var config = require('./src/server/config.js');
var passwd = require('./src/server/util/passwd.js');
var app = express();
var Kasino = require('./src/server/game/kasino.js');

var PLAYERS_KEY = 'players';
const JWT_SECRET = '3otWu9jW3jZK2moCK0831soJpx';

// Init part
// set up express
app.set('port', (process.env.PORT || 4000));

app.use('/', express.static(path.join(__dirname, 'dist')));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));

app.disable('etag');
if (!process.env.ISPROD) app.use(morgan('dev'));

app.use(function(req, res, next) {
    // CORS: dev rules
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'PUT, POST, GET');
    res.setHeader('Access-Control-Allow-Headers', 'Content-Type, Authorization');

    // Disable caching so we'll always get the latest comments.
    res.setHeader('Cache-Control', 'no-cache, no-store, must-revalidate');
    res.setHeader('Pragma', 'no-cache');
    res.setHeader('Expires', '0');
    next();
});

// JWT for non-public endpoints
app.use(expressJwt({ secret: JWT_SECRET, algorithm: 'HS256'}).unless({
  path: ['/session/login', '/api/hiscores', '/index.html', '/bundle.js',
    { url: /^\/api\/player\/[A-z]+/, methods: ['OPTIONS', 'PUT']}]}));

// Storage setup
storage.initSync({
  dir: 'storage',
  logging: true,
  interval: false,
  continuous: true,
  ttl: false
});

// Keeping always players in memory, initial load or setup
var players = storage.getItemSync('players');
if (!players) {
  console.log("Resetting defaul users...");
  storage.setItemSync(PLAYERS_KEY, []);
} else {
  console.log('Loaded ' + players.length +' players.');
}

var games = [];
var gameId = 1;

// Init empty sessions, locked login attempts etc
var sessions = [];
var loginFails = [];
var lastHiscoreUpdate = 0;
var hiscores = [];

app.post('/api/game', function(req, res) {
  if (undefined == req || !req.user || !req.user.login) {
    return res.status(401).send();
  }
  const playerName = req.user.login;
  game = {
    'gameId': ++gameId,
    'kasino': new Kasino()
  }
  res.json({ status: 'gameId: ' + gameId });
});

app.put('/api/player/:playername', function(req, res) {
  var player = req.params.charname;
  var playerdatapos = _.findIndex(players, ['login', player]);
  if (playerdatapos !== -1)
  {
    return res.status(400).send({ status: 'Player already present' });
  }
  try {
    const newPlayer = req.body.playerDetails;
    players.push(charObj);
    storage.setItem(PLAYERS_KEY, players);
    res.json({ status: 'Player created ok'});
  } catch (err) {
    console.log('Player creation "'+ player +'" : ' + err)
    res.status(400).send({ status: 'Illegal call' });
  }
});

 function isValidSession(login, req) {
  if (!req || !login || !req.user || req.user.login !== login) return false;
  return true;
}

app.get('/api/player/:playername', function(req, res) {
  var player = req.params.playername;
  if (!isValidSession (player, req)) {
    return res.status(401).send();
  }
  var playerdatapos = _.findIndex(players, ['login', char]);
  if (playerdatapos !== -1) res.json(players[playerdatapos]);
    else res.status(404);
});

app.get('/api/hiscores', function(req, res) {
  if (Date.now() > lastHiscoreUpdate + 30 * 1000) // Update twice a minute
  {
    hiscores = characters.map(function(el) {
      return { login: el.login, score: el.exp };
    });
    hiscores.sort(function(a, b) {
      return (a.totalExp + a.exp) - (b.totalExp + b.exp);
    });
    lastHiscoreUpdate = Date.now();
  }
  var login = req.query.login;
  var num = parseInt(req.query.num) ? req.query.num : 10;

  var resultScores = _.take(hiscores, num);

  if (login && login.length) {
    var ownscorepos = _.findIndex(hiscores, ['login', login]);
    if (ownscorepos != -1) {
      for (var i = Math.max(ownscorepos - 2, num);
               i < Math.min(ownscorepos + 2, hiscores.length);
               i++) {
        var ownscore = hiscores[i];
        ownscore.pos = i + 1;
        resultScores.push(ownscore);
      }
    }
  }

  res.json(resultScores);
});

app.post('/session/login', function(req, res) {
  if (!req.body.login || !req.body.password) {
    return res.status(400).send('Login and password both must be present');
  }

  try {
    if (!config.validName.test(req.body.login))
      return res.status(401).send('Illegal login name!');

    // ...should the account be locked because of failed logins
    var loginFail = _.find(loginFails, { 'login': req.body.login });
    if (loginFail) {
      if (loginFail.firstFailDate < (Date.now() - config.loginFailWindow)) {
      // There were failed logins before, but the locked status should be
      // now removed to allow new tries
        _.remove(loginFails, { 'login': req.body.login});
      } else {
        if (loginFail.failCount >= config.loginFailAttempts) {
          console.log('Too many failed logins for ' + req.body.login);
          throw new Error('Too many failed logins, wait!');
        }
      }
    }
    if (!config.validPasswd.test(req.body.password))
      throw new Error('Illegal password');
    var user = _.find(players, { 'login' : req.body.login });
    if (!user) throw new Error ('User not found');
    if (true !== passwd.isValidPasswd(req.body.password,
      user.passwdData.passwdHash, user.passwdData.salt))
      throw new Error('Wrong password');
  } catch (err) {
    // Ok, the login failed for any specific reason...
    console.log('Error in login: ' + err);
    // Create or increase counter for failed logins
    var loginFail = _.find(loginFails, { 'login': req.body.login });
    if (loginFail) {
      loginFail.failCount++;
    } else {
      loginFails.push({
        login: req.body.login,
        firstFailDate: Date.now(),
        failCount: 1
      });
    }
    // Return generic error regardless of root reason
    return res.status(401).send('Login failed.');
  }

  // So login was ok...

  var token = jwt.sign({ login: req.body.login }, JWT_SECRET,
    { algorithm: 'HS256', expiresIn: '1m' });
  res.json({ user: req.body.login, token: token });
});

app.post('/session/logout', function (req, res) {
  res.send({ logoutSuccess: true });
});

app.listen(app.get('port'), function() {
  console.log('Server started: http://localhost:' + app.get('port') + '/');
});
