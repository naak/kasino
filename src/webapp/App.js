import React from 'react';
import { Provider } from 'react-redux';
import store from './util/store';
import Main from './Main';

export default function App(props) {
  return (
    <Provider store={store}>
      <Main/>
    </Provider>
  );
}
