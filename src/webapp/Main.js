import React from 'react';
import { connect } from 'react-redux';
import { Col, Row, Grid } from 'react-bootstrap';
import Login from './Login';
import Status from './Status';

class Main extends React.Component {
  render() {
    const { login } = this.props;
    return (
      <div id="cont">
        <Grid>
          <Row>
            <Col className="App-main" xs={12} sm={8}>
              {!login.loggedIn ? <Login/> : <Status/>}
            </Col>
          </Row>
        </Grid>
      </div>
    )
  }
}

function mapStateToProps(state) {
  return {
    login: state.login
  };
}

export default connect(mapStateToProps)(Main);
