import React from 'react';
import { connect } from 'react-redux';
import { Field, reduxForm, propTypes } from 'redux-form';
import { Row, Col } from 'react-bootstrap';
import Button from 'react-bootstrap/lib/Button';
import { doLogin } from './util/actions';
import 'bootstrap/dist/css/bootstrap.css';

class Login extends React.Component {
  handleSubmit(values, dispatch) {
    dispatch(doLogin(values.login, values.password));
  }

  render() {
    const login = this.props.login;
    return (
      <Row className='Login'>
        <div className='main-login'>Enter game:</div>
        <form className='loginForm' onSubmit={this.props.handleSubmit(this.props.onSubmit)}>
          <Row className='login-credential-row'>
            <Col xs={5} className='loginField'>
              <Field name='login' component='input'
                placeholder='Character name'
                label='Character'/>
                {login.error && (
                  <strong>{login.error}</strong>
                )}
            </Col>
            <Col xs={5}>
              <Field name='password' component='input' type='password'
                placeholder='password'
                label='Password'/>
            </Col>
            <Col xs={2}>
              <Button bsSize='small' bsStyle='primary'
                disabled={login.isLoggingIn}
                type='submit'>Log in{login.error}</Button>
            </Col>
          </Row>
          {login.msg && (
            <Row className="loginFail">
              <strong>Login failed: {login.msg}</strong>
            </Row>
          )}
        </form>
      </Row>
    );
  }
}

function validate(values) {
  var error = {};
  if (!values.login || values.login.length < 5)
  error.login = 'Too short login';
  if (!values.password || values.password.length < 1)
  error.password = 'Password is required';
  return error;
}

function mapStateToProps(state) {
  return {
    login: state.login,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    onSubmit: values => { dispatch(doLogin(values.login, values.password)); }
  }
}

Login = reduxForm({
  form: 'login',
  validate
})(Login);

export default connect(mapStateToProps, mapDispatchToProps)(Login);
