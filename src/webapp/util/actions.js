export const requestLogin = login => {
  return {
    type: 'REQUEST_LOGIN',
    login
  };
}

export const receiveLogin = json => {
  return {
    type: 'RECEIVE_LOGIN',
    loginAt: Date.now(),
    jwt: json.token
  };
}

export const receiveLoginFailure = err => {
  return {
    type: 'RECEIVE_LOGIN_FAILURE',
    msg: (err ? err : 'Network service error')
  };
}

export const requestLogout = () => {
  return {
    type: 'REQUEST_LOGOUT'
  };
}

const doLoginRequest = (login, password) => dispatch => {
  dispatch(requestLogin(login));
  return fetch('http://localhost:4000/session/login', {
    method: 'POST',
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json'
    },
    body: JSON.stringify ({
      login: login,
      password: password
    })
  })
  .then(response => {
    if (response.status >= 400) throw new Error(response.statusText);
    response.json().then(data => {
      dispatch(receiveLogin(data));
    })
  })
  .catch(err => dispatch(receiveLoginFailure(err.message)));
}

export const doLogin = (login, password) => (dispatch, getState) => {
  var state = getState();
  if (!state.isLoggingIn) return dispatch(doLoginRequest(login, password));
}
