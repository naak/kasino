import { reducer as formReducer } from 'redux-form';
import { combineReducers } from 'redux';

const loginReducer = ( state = {loggedIn: false}, action) => {
  switch(action.type) {
    case 'REQUEST_LOGIN':
      return {
          ...state,
          login: action.login,
          isLoggingIn: true
      };
    case 'RECEIVE_LOGIN':
      return {
        ...state,
        jwt: action.jwt,
        isLoggingIn: false,
        loggedIn: true,
        loginAt: action.loginAt
      };
    case 'RECEIVE_LOGIN_FAILURE':
      return {
        ...state,
        msg: action.msg,
        isLoggingIn: false
      };
    case 'REQUEST_LOGOUT':
      return {
        isLoggingIn: false,
        loggedIn: false,
        jwt: '',
      };
    default:
      return state;
  }
}

const rootReducer = combineReducers(
  {
    login: loginReducer,
    form: formReducer
  }
)

export default rootReducer;
