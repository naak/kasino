import React from 'react';
import { connect } from 'react-redux';

class Status extends React.Component {
  logoutButton = (event) => {
    event.preventDefault();
  };

  render() {
    const { login } = this.props;
    return (
      <div>
        Moi {login.login}<br/>
        <button onClick={this.logoutButton}>Logout</button>
      </div>
    )
  }
}

function mapStateToProps(state) {
  return {
    login: state.login
  };
}

function mapDispatchToProps(dispatch) {
  return {
    dispatch
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(Status);
