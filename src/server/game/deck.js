var _ = require('lodash');

module.exports = class Deck {
  constructor() {
    let toShuffle = [];
    this._deck = [];
    for (var suite of 'HDCS') {
      for (var value of '23456789TJQKA') {
        toShuffle.push(value + suite);
      }
    }
    while (toShuffle.length > 0) {
      let card = _.sample(toShuffle);
      _.pull(toShuffle, card);
      this._deck.push(card);
    }
  }

  draw(num) {
    if (num < 0) throw new Error('Illegal argument')
    if (this._deck.length < num) throw new Error('Deck ran out');
    return this._deck.splice(0, num);
  }

  cardsLeft() {
    return this._deck.length > 0 ? true : false;
  }

  static value(card, inHand) {
    if(card == undefined || card.length != 2) {
      throw new Error('Invalid card');
    }
    // Check for special cards first
    if (card === '2S') return inHand === true ? 15 : 2;
    if (card === 'TD') return inHand === true ? 16 : 10;

    switch (card[0]) {
      case 'T': { return 10; break; }
      case 'J': { return 11; break; }
      case 'Q': { return 12; break; }
      case 'K': { return 13; break; }
      case 'A': { return inHand === true ? 14 : 1; break; }
    }
    if (card[0] >= '2' && card[0] <= '9') return card[0] - '0';
  }
}
