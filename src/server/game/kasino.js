var Deck = require('./deck.js');
var _ = require('lodash');

module.exports = class Kasino {
  constructor(players) {
    if (players == undefined || players.length < 2 || players.length > 4) {
      throw new Error("Illegal amount of players");
    }
    this._starterOfCurrentRound = -1; //Increases by one in beginning...
    this._carrySpade = 0;
    this._carryCard = 0;
    this._players = [];
    for(const player of players) {
      const pdata = {
        'name': player,
        'points': 0,
        'houses': 0
      }
      this._players.push(pdata);
    }
    this.startNewDeck();
  }

  startNewDeck() {
    this._deck = new Deck();
    this._finished = false;
    this._starterOfCurrentRound++;
    this._round = 1;
    this._table = this._deck.draw(4);
    this._playerInTurn = this._starterOfCurrentRound;
    this._players.forEach((x) => {
      x.pile = [];
      x.hand = this._deck.draw(4);
    });
  }

  getGameData(playerIdx) {
    return {
      'inTurn' : this._playerInTurn,
      'starter' : this._starterOfCurrentRound,
      'finished' : this._finished,
      'round' : this._round,
      'cardsLeft' : this._deck.cardsLeft(),
      'carrySpade' : this._carrySpade,
      'carryCard' : this._carryCard,
      'players' : this._players.map((x) => {
        return _.pick(x, 'name', 'points', 'houses')
      }),
      'hand' : this._players[playerIdx].hand,
      'table' : this._table
    }
  }

  put(handIndex) {
    if (this._finished === true) return false;
    if (handIndex == undefined) {
      throw new Error("Missing index data");
    }

    if (handIndex >= this._players[this._playerInTurn].hand.length || handIndex < 0) {
      throw new Error("Illegal hand index");
    }

    this._table = this._table.concat(this._players[this._playerInTurn].hand.splice(handIndex, 1));

    this.afterMoveActions();

    return true;
  }

  get(handIndex, tableIndices) {
    if (this._finished === true) return false;
    if (handIndex == undefined || tableIndices.constructor !== Array || tableIndices.length === 0) {
      throw new Error("Missing index data");
    }

    //´Check hand index legality and get the card value
    if (handIndex >= this._players[this._playerInTurn].hand.length || handIndex < 0) {
      throw new Error("Illegal hand index");
    }
    const hValue = Deck.value(this._players[this._playerInTurn].hand[handIndex], true);

    // Check table indices legality and their card values
    const tValues = [];
    for (let ti of tableIndices) {
      if (ti >= this._table.length || ti < 0) {
        throw new Error("Illegal table index");
      }
      tValues.push(Deck.value(this._table[ti], false));
    }

    let getDone = false;
    if (this.isLegalGet(hValue, tValues)) {
      this._players[this._playerInTurn].pile =
        this._players[this._playerInTurn].pile.concat(
          this._players[this._playerInTurn].hand.splice(handIndex, 1));
      for (let ti of tableIndices) {
        this._players[this._playerInTurn].pile =
          this._players[this._playerInTurn].pile.concat(this._table.splice(ti, 1));
      }
      this.afterMoveActions();
      getDone = true;
    }

    return getDone;
  }

  afterMoveActions() {
    // Check for possible house
    if (this._table.length === 0 && this._deck.cardsLeft() === true) {
      this._players[this._playerInTurn].houses++;
    }

    // Next player in turn
    this._playerInTurn = (this._playerInTurn + 1) % this._players.length;

    // Check for end of round, happend when empty hand for first player encountered
    if (this._playerInTurn === this._starterOfCurrentRound &&
        this._players[this._playerInTurn].hand.length === 0) {
      // Is this the end, calc round scores and sum up if so
      if (this._deck.cardsLeft() === false) {
        this._lastRoundScore = this.calcScore();
        for (let i = 0; i < this._players.length; i++) {
          this._players[i].points += this._lastRoundScore[i];
        }
        this._finished = true;
      }
      else {
        // Draw new hands for everyone, game continues
        for (let i = 0; i < this._players.length; i++) {
          this._players[i].hand = this._deck.draw(4);
        }
        this._round++;
      }
    }
  }

  // Calc points in each players piles
  // If round is not finished, the card amount scores are based on current situation
  calcScore() {
    const score = new Array(this._players.length).fill(0);
    let hiSpades = 0;
    let hiSpadePlayer = 0;
    let spadeTie = false;
    let hiCards = 0;
    let hiCardPlayer = 0;
    let cardTie = false;

    for (let i = 0; i < this._players.length; i++) {
      // One point for each ace or 2S collected, two points for TD
      score[i] += this._players[i].pile.reduce((n, val) => {
        return n + ('A' === val[0] || '2S' === val || ('TD' === val) * 2);
      }, 0);

      // Does this player have more spades than previously found highest amount
      let spadeCount = this._players[i].pile.reduce((n, val) => {
        return n + ('S' === val[1]);
      }, 0);
      if (spadeCount > hiSpades) {
        hiSpadePlayer = i;
        hiSpades = spadeCount;
        spadeTie = false;
      }
      else if (spadeCount === hiSpades) {
        spadeTie = true;
      }

      // Does this player have more cards than previously found highest amount
      if (this._players[i].pile.length > hiCards) {
        hiCardPlayer = i;
        hiCards = this._players[i].pile.length;
        cardTie = false;
      }
      else if (this._players[i].pile.length === hiCards) {
        cardTie = true;
      }
    }

    // Spade awards 2 points, on tie the points are carried to next round
    if (spadeTie === false) {
      score[hiSpadePlayer] += 2 + this._carrySpade;
      this._carrySpade = 0;
    }
    else {
      this._carrySpade += 2;
    }

    // Cards award 1 point, on tie the point is carried to next round
    if (cardTie === false) {
      score[hiCardPlayer] += 1 + this._carryCard;
      this._carryCard = 0;
    }
    else {
      this._carryCard += 1;
    }

    return score;
  }

  isLegalGet(hValue, tValues) {
    let validGet = false; // Has a valid way to pick all selected cards been found

    // First loop thru all different set part lengths partitions for table indices (1 .. n)
    for (let i = 1; i <= tValues.length && validGet === false; i++) {
      let set = this.setOfPartitions(tValues, i);

      // Loop thru all set combinations for this max set part length
      for (let a = 0; a < set.length && validGet === false; a++) {

        //  Find out if all set parts within this set match the hand value
        let allSetsMatch = true;
        for (let b = 0; b < set[a].length; b++) {
          let setSum = 0;
          for (let c = 0; c < set[a][b].length; c++) {
            setSum += set[a][b][c];
          }
          if (setSum !== hValue) allSetsMatch = false;
        }

        // If all set parts matched, we can quit looking for...
        if (allSetsMatch === true) validGet = true;
      }
    }
    return validGet
  }

  // Return all combinations of sets from list, having len length components
  setOfPartitions(list, len) {
    let ret = [];
    if (list.length < len || len < 1) return ret;

    if (len === 1) {
      return [[list]];
    }

    // list - 1, len
    let previous = this.setOfPartitions(list.slice(0, list.length - 1), len);
    for (let i = 0; i < previous.length; i++) {
      for (let j = 0; j < previous[i].length; j++) {
        // TODO: Not so elegant way to deep copy the array of objects
        let tmp = JSON.parse(JSON.stringify(previous[i]));
        tmp[j].push(list[list.length - 1]);
        ret.push(tmp);
      }
    }

    // list -1, len - 1
    let set = [list[list.length - 1]];
    let previous2 = this.setOfPartitions(list.slice(0, list.length - 1), len - 1);
    for (let i = 0; i < previous2.length; i++) {
      let tmp = previous2[i];
      tmp.push(set);
      ret.push(tmp);
    }

    return ret;
  }

  debugPrint() {
    console.log('Round: ' + this._round + ' / Finished: ' + this._finished);
    console.log('In turn: ' + this._players[this._playerInTurn]['name']);
    console.log('Carries (c/s): ' + this._carryCard + '/' + this._carrySpade);
    console.log('Table: ' + this._table);
    console.log(this._players);
  }
}
