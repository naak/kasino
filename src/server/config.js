const validName = /^[a-z]{5,10}$/;
const validEmail =  /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
const validPasswd = /^[A-z0-9\!~<>,;:_=?*+#."&§%°()\|\[\]\-\$\^\@\/]{8,32}$/;

const loginFailWindow = 1 * 60 * 1000;
const loginFailAttempts = 5;

module.exports.validName = validName;
module.exports.validEmail = validEmail;
module.exports.validPasswd = validPasswd;
module.exports.loginFailWindow = loginFailWindow;
module.exports.loginFailAttempts = loginFailAttempts;
