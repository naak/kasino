'use strict';
var crypto = require('crypto');

 var genRandomString = function(length) {
   return crypto.randomBytes(Math.ceil(length / 2))
   .toString('hex').slice(0, length);
 };

var sha512 = function(passwd, salt) {
    var hash = crypto.createHmac('sha512', salt); /** Hashing algorithm sha512 */
    hash.update(passwd);
    var value = hash.digest('hex');
    return {
        salt:salt,
        passwdHash:value
    };
};

function hashPasswd(passwd) {
    var salt = genRandomString(32); // Random salt of 32 characters
    return sha512(passwd, salt);
}

function isValidPasswd(givenPasswd, storedPasswdHash, storedSalt) {
  var passwdData = sha512(givenPasswd, storedSalt);
  return (storedPasswdHash === passwdData.passwdHash ? true : false);
}

module.exports.hashPasswd = hashPasswd;
module.exports.isValidPasswd = isValidPasswd;
