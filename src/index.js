import React from 'react';
import ReactDOM from 'react-dom';
import App from './webapp/App';

ReactDOM.render(
  <App/>,
  document.getElementById('root')
);
