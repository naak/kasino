chai = require('chai');
assert = chai.assert;

Deck = require('../../../src/server/game/deck.js');

describe('Deck', () => {
  dtest = new Deck();
  it('Initializes ok', () => {
    assert(dtest, Deck, 'The test deck is an instance of Deck')
    assert.isTrue(dtest.cardsLeft());
  });

  it('Exception scenarios', () => {
    ret = dtest.draw(0);
    assert.deepEqual(ret, [], 'Draw(0) returns empty array');
    assert.throws(() => { dtest.draw(-1) }, Error, 'Illegal argument');
    assert.throws(() => { dtest.draw(53) }, Error, 'Deck ran out');
  });

  it('draw tests', () => {
    ret = dtest.draw(1);
    assert.lengthOf(ret, 1, 'Draw(1) has length of one');
    assert.isTrue(dtest.cardsLeft());
    ret = dtest.draw(51);
    assert.lengthOf(ret, 51, 'Draw(51) has length of 51');
    for (let val of ret) assert.match(val, /^[2-9TJQKA][SCDH]$/, 'Card has valid form');
    assert.isFalse(dtest.cardsLeft());
    assert.throws(() => { dtest.draw(1) }, Error, 'Deck ran out');
  });

  it('value tests', () => {
    // Special card scenarios
    assert.equal(Deck.value('AS', true), 14);
    assert.equal(Deck.value('AS', false), 1);
    assert.equal(Deck.value('2S', true), 15);
    assert.equal(Deck.value('2S', false), 2);
    assert.equal(Deck.value('TD', true), 16);
    assert.equal(Deck.value('TD', false), 10);
    // Just basic number cards in hand or not
    assert.equal(Deck.value('6H', true), 6);
    assert.equal(Deck.value('7C', false), 7);
    // Just high number cards in hand or not
    assert.equal(Deck.value('JH', true), 11);
    assert.equal(Deck.value('QC', false), 12);
    assert.equal(Deck.value('KS', false), 13);
  });
})
