chai = require('chai');
sinon = require('sinon');
assert = chai.assert;

Kasino = require('../../../src/server/game/kasino.js');

describe('Kasino', () => {
  it('Initialize failures', () => {
    assert.throws(() => { new Kasino(); }, Error, /Illegal amount of players/);
    assert.throws(() => { new Kasino(['1', '2', '3', '4', '5']); }, Error, /Illegal amount of players/);
    assert.throws(() => { new Kasino(['1']); }, Error, /Illegal amount of players/);
  });

  it('Initializes ok', () => {
    assert.doesNotThrow(() => { new Kasino(['1', '2']); });
    assert.doesNotThrow(() => { new Kasino(['1', '2', '3', '4']); });
  });

  it('Game of 2 players simulation', () => {
    let dstate = 0;
    let dcount = 0;
    let cardsLeft = 52;
    const vals = '23456789TJQK';
    sinon.stub(Deck.prototype, 'draw').callsFake((num) => {
      cardsLeft -= 4; // Game always draws 4 a time
      // Table
      if (dstate === 0) {
        dstate = 1;
        return ['AS', 'AH', 'AC', 'AD'];
      }

      if (dstate === 3) {
        dcount++;
        dstate = 1;
      }
      dstate++;

      // Players in rounds of 4, give always same cards for both players
      // Avoiding 2S and TD to allow easy automated game
      return [vals[dcount] + 'C', vals[(dcount + 1) % 13] + 'H',
        vals[(dcount + 2) % 13] + 'C', vals[(dcount + 3) % 13] + 'H'];
    });

    sinon.stub(Deck.prototype, 'cardsLeft').callsFake(() => {
      return cardsLeft === 0 ? false : true;
    });

    ktest = new Kasino(['a', 'b']);
    for (let i = 0; i < (52 - 4) / 8; i++) { // Rounds of 8 new cards each
      for (let j = 0; j < 4; j++) {
        // First player puts first card and second picks it up with same value card
        assert.doesNotThrow(() => { ktest.put(0); }, 'Put card as player 0');
        assert.isTrue(ktest.get(0, [4]), 'Get card as player 1');
      }
    }
    // Game should be finished
    assert.isFalse(ktest.put(0), 'Not able to make move after finished Game');
    let kdata = ktest.getGameData(1);
    assert.equal(kdata.finished, true, 'Game should be Finished');
    assert.equal(kdata.cardsLeft, false, 'No cards should be left');
    assert.equal(kdata.players[0].points, 0, 'No points for player 0');
    assert.equal(kdata.players[1].points, 1, 'Card point for player 1');
    assert.equal(kdata.carrySpade, 2, 'Two points for tied spade  ');
    assert.equal(kdata.carryCard, 0, 'No tie for cards');

    // Start next deck, get rid of the game simulation deck stubs first
    Deck.prototype.draw.restore();
    Deck.prototype.cardsLeft.restore();
    assert.doesNotThrow(() => { ktest.startNewDeck(); });
    kdata = ktest.getGameData(0);
    assert.equal(kdata.inTurn, 1, 'Player 1 starts second deck');
    assert.equal(kdata.finished, false, 'Game is ongoing');
    assert.lengthOf(kdata.table, 4, 'Table has been delt');
    assert.lengthOf(kdata.hand, 4, 'Hand has been delt');

  });

  it('startNewDeck for 4 players', () => {
    ktest = new Kasino(['a', 'b', 'c', 'd']);
    ktest.startNewDeck();
    assert.doesNotThrow(() => { kdata = ktest.getGameData(3); });
    assert.lengthOf(kdata.hand, 4, 'Hand has been delt');
    assert.lengthOf(kdata.table, 4, 'Hand has been delt');
    assert.equal(kdata.inTurn, 1, 'Player 1 starts second deck');
  });

  it('isLegalGet tests', () => {
    ktest = new Kasino(['a', 'b']);
    assert.isTrue(ktest.isLegalGet(10, [5, 5, 10, 3, 7]));
    assert.isFalse(ktest.isLegalGet(10, [5, 5, 11, 3, 7]));
    assert.isFalse(ktest.isLegalGet(10, []));
    assert.isTrue(ktest.isLegalGet(5, [5]));
    assert.isTrue(ktest.isLegalGet(16, [1, 15]));
    assert.isTrue(ktest.isLegalGet(2, [2, 2, 2]));
    assert.isFalse(ktest.isLegalGet(4, [2, 2, 2]));
  })
});
